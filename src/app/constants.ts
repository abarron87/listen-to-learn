export class AppData {
    public static QUESTIONS = {
        howMany: 'How many words do you hear?',
        whatDoYouHear: 'Write the words you think you hear.',
        whatDoYouHearSound: 'Write down the written form of what you hear.'
    };

    public static ICONS = {
        pause: 'pause',
        play: 'play_arrow',
        slowMoPlay: 'slow_motion_video'
    };

    public static SLOW_MO_SPEED = .6;

    public static TEST_QUESTION_TYPES = ['multipleChoice', 'writingPractice', 'pronunciationPractice'];
}

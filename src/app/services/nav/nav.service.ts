import { Injectable } from '@angular/core';
import { SharedDataService } from '../shared-data/shared-data.service';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class NavService {

    constructor(private sharedDataService: SharedDataService, private route: ActivatedRoute) {
        console.log('navService route param phrase', this.route.snapshot.paramMap.get('phraseId'));
        console.log('navService route param sound', this.route.snapshot.paramMap.get('soundId'));
    }

    getQuestionRoutePath(questionType, soundKey) {
        const phraseKey = this.sharedDataService.get('currentPhrase').$key || this.route.snapshot.paramMap.get('phraseId'); // already setting the phrase object in sound-to-study. So just grab key from it.
        // const soundKey = !!this.sharedDataService.get('currentSoundKey') ? this.sharedDataService.get('currentSoundKey') : this.route.snapshot.paramMap.get('soundId'); // make sure it's the sound of next question.

        switch (questionType) {
            case 'multipleChoice':
                return ['/test/multiple-choice', phraseKey, soundKey];
            case 'writingPractice':
                return ['/test/what-do-you-hear', phraseKey, soundKey];
            case 'pronunciationPractice':
                return ['/test/pronunciation-practice', phraseKey, soundKey];
        }
    }

    getStudyRoutePath(soundKey) {
        return ['/sounds-to-study', this.sharedDataService.get('currentPhrase').$key || this.route.snapshot.paramMap.get('phraseId'), soundKey];
    }

}

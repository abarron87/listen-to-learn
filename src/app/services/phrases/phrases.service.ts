import { Injectable } from '@angular/core';
import { Phrase } from '../../types/phrase';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class PhrasesService {

    constructor(private db: AngularFireDatabase) { }

      /*
        getPhraseByKey

        Looks up a phrase from the /phrases node by its key.
        Sends the results back to the caller of the method.
    */
    getPhraseByKey(phraseKey, callback) {
        const phrasesRef = this.db.database.ref('/phrases');

        phrasesRef.orderByKey().equalTo(phraseKey).on('child_added', snapshot => {
            const key = snapshot.key;
            const phrase = snapshot.val();

            callback(new Phrase(
                key,
                phrase.writtenForm,
                phrase.audio,
                phrase.video,
                phrase.explanation,
                phrase.numWords
            ));
        });
    }
}

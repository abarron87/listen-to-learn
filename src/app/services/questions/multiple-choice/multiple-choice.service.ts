import { Injectable } from '@angular/core';
import { MultipleChoice } from '../../../types/multiple-choice';
import { AngularFireDatabase } from 'angularfire2/database';
import * as _ from 'lodash';

@Injectable()
export class MultipleChoiceService {

    constructor(private db: AngularFireDatabase) { }

    getChoicesBySoundKey(soundKey: string, callback) {
        const multipleChoiceRef = this.db.database.ref('/multipleChoice');

        multipleChoiceRef.orderByKey().equalTo(soundKey).on('child_added', snapshot => {
            const key = snapshot.key;
            const multipleChoice = snapshot.val();

            callback(new MultipleChoice(
                key,
                multipleChoice.correct,
                multipleChoice.incorrect
            ));
        });
    }

    isAnswerCorrect(userAnswer: string, choiceObj: MultipleChoice) {
        return !!choiceObj.correct[userAnswer];
    }

    getChoicesBySoundKeys(soundKeys: string[], callback) {
        const choices: MultipleChoice[] = [];
        _.forEach(soundKeys, key => {
            this.getChoicesBySoundKey(key, mChoice => {
                choices.push(mChoice);

                if (choices.length === soundKeys.length) {
                    callback(choices);
                }
            });
        });
    }

}

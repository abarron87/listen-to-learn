import { TestBed, inject } from '@angular/core/testing';

import { MultipleChoiceService } from './multiple-choice.service';

describe('MultipleChoiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MultipleChoiceService]
    });
  });

  it('should be created', inject([MultipleChoiceService], (service: MultipleChoiceService) => {
    expect(service).toBeTruthy();
  }));
});

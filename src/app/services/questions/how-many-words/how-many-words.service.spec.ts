import { TestBed, inject } from '@angular/core/testing';

import { HowManyWordsService } from './how-many-words.service';

describe('HowManyWordsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HowManyWordsService]
    });
  });

  it('should be created', inject([HowManyWordsService], (service: HowManyWordsService) => {
    expect(service).toBeTruthy();
  }));
});

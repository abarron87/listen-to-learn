import { Injectable } from '@angular/core';

@Injectable()
export class HowManyWordsService {

//   getQuestion(id) {
//       const question: Question = {
//           question: 'bla bla',
//           audio: 'my audio',
//           acceptedAnswers: [6],
//           correctAnswer: 5
//       };

//       return question;
//   }

    /*
        isAnswerCorrect

        Return whether or not the user's given answer matches the correct answer.
        @param userAnswer - the user's answer.
        @param phrase - a Phrase object.

        @return boolean.
    */
    isAnswerCorrect(userAnswer, phrase): boolean {
        return userAnswer === phrase.numWords.correct;
    }

    /*
        isAnswerAccepted

        Return whether or not the user's given answer counts as an accepted answer.
        @param userAnswer - the user's answer.
        @param phrase - a Phrase object.

        @return boolean.
    */
    isAnswerAccepted(userAnswer, phrase): boolean {
        return phrase.numWords.accepted.includes(userAnswer);
    }
}

import { TestBed, inject } from '@angular/core/testing';

import { TestQuestionsService } from './test-questions.service';

describe('TestQuestionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TestQuestionsService]
    });
  });

  it('should be created', inject([TestQuestionsService], (service: TestQuestionsService) => {
    expect(service).toBeTruthy();
  }));
});

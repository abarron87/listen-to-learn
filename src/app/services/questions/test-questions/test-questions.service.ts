import { Injectable } from '@angular/core';
import { TestQuestionsState } from '../../../types/test-questions-state';
import { SharedDataService } from '../../shared-data/shared-data.service';
import { PhrasesService } from '../../phrases/phrases.service';
import { SoundsService } from '../../../sounds.service';
import { MultipleChoiceService } from '../multiple-choice/multiple-choice.service';
import { WhatDoYouHearService } from '../what-do-you-hear/what-do-you-hear.service';
import { Phrase } from '../../../types/phrase';
import { Sound } from '../../../types/sound';
import { PronunciationsService } from '../pronunciations/pronunciations.service';
import { Pronunciation } from '../../../types/pronunciation';
import { MultipleChoice } from '../../../types/multiple-choice';
import { Subject } from 'rxjs/Subject';
import { NavService } from '../../nav/nav.service';
import { Question } from '../../../types/question';
import * as _ from 'lodash';
import { AppData } from '../../../constants';

@Injectable()
export class TestQuestionsService {
    state: TestQuestionsState;
    phrase: Phrase;
    sounds: Sound[];
    soundKeys: string[];
    questionTypes: string[];
    nextQuestion: any;
    currentQuestion: any;
    currentSound: Sound;

    dataCount: number;
    ready: boolean;
    ready$ = new Subject<boolean>(); // look into different types of Subject. Async, Behavior, etc

    constructor(
        private sharedDataService: SharedDataService, private phrasesService: PhrasesService,
        private soundsService: SoundsService, private multipleChoiceService: MultipleChoiceService,
        private whatDoYouHearService: WhatDoYouHearService, private pronunciationsService: PronunciationsService,
        private navService: NavService
    ) {
        this.state = new TestQuestionsState();
        this.questionTypes = AppData.TEST_QUESTION_TYPES;
    }

    initState(phraseKey) {
        this.dataCount = 0;
        // put the sound IDs into each of the question type arrays of the TestQuestionsState object.

        // get phrase
        // get all sounds
        // get all test question data using sound key to lookup.
        // populate this.state

        // const cachedPhrase = this.sharedDataService.get('currentPhrase');

        // if (!!cachedPhrase) {
        //     this.phrase = cachedPhrase;
        // } else {
        //     this.phrasesService.getPhraseByKey(phraseKey, phrase => this.phrase = phrase);
        // }

        this.soundsService.getAllSoundsByPhrase(phraseKey, (sounds, soundKeys) => {
            console.log('Sounds', sounds);
            this.sounds = sounds;
            this.soundKeys = soundKeys;

            // writing practice simply checks the answer against the written form on the sound node.
            this.state.writingPracticeQuestions = soundKeys;

            this.multipleChoiceService.getChoicesBySoundKeys(
                this.soundKeys, (choices: MultipleChoice[]) => {
                    this.state.multipleChoiceQuestions = choices;

                    // don't like doing this as a callback but don't know how to detect when all is ready otherwise.
                    // TODO create observable.
                    this.pronunciationsService.getPronunciationsBySoundKeys(
                        this.soundKeys, (pronunciations: Pronunciation[]) => {
                            this.state.pronunciationPracticeQuestions = pronunciations;
                            // this.state.currentQuestionType = 'multipleChoice';
                            this.ready = true; // for future components who require the service.
                            this.ready$.next(true);
                        }
                    );
                }
            );
        });
    }

    getSetNextQuestion() {
        if (this.doQuestionsRemain()) {
            const randomIndexForQType = (Math.floor(Math.random() * this.questionTypes.length) + 1) - 1;
            const questionType = this.questionTypes[randomIndexForQType];
            const randomIndexForQ = (Math.floor(Math.random() * this.state[`${questionType}Questions`].length) + 1) - 1;
            console.log('random number 1:', randomIndexForQType);
            console.log('random number 2:', randomIndexForQ);
            console.log('questionType:', questionType);
            let qPopped: any;

            qPopped = this.state[`${questionType}Questions`].splice(randomIndexForQ, 1)[0];
            console.log('qPopped:', qPopped);
            this.soundsService.cacheSoundKey('nextSound', qPopped.$key || qPopped);

            // I THINK THIS IS ACTUALLY 'NEXT QUESTION TYPE'. TRY AND STORE BOTH.
            this.state.nextQuestionType = this.questionTypes[randomIndexForQType];
            if (!this.state[`${questionType}Questions`].length) {
                console.log(`${questionType} questions finished. Removing...`);
                this.questionTypes.splice(randomIndexForQType, 1);
            }

            // try and get question from the next question type.
            // if there are no questions left from that question type, move to the next.
            // if (this.state.currentQuestionType === 'multipleChoice') {
            //     if (!!this.state.writingPracticeQuestions.length) {
            //         qPopped = this.state.writingPracticeQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped);
            //         this.state.currentQuestionType = 'writingPractice';
            //     } else if (!!this.state.pronunciationPracticeQuestions.length) {
            //         qPopped = this.state.pronunciationPracticeQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped.$key);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped.$key);
            //         this.state.currentQuestionType = 'pronunciationPractice';
            //     } else {
            //         qPopped = this.state.multipleChoiceQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped.$key);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped.$key);
            //         this.state.currentQuestionType = 'multipleChoice';
            //     }
            // } else if (this.state.currentQuestionType === 'writingPractice') {
            //     if (!!this.state.pronunciationPracticeQuestions.length) {
            //         qPopped = this.state.pronunciationPracticeQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped.$key);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped.$key);
            //         this.state.currentQuestionType = 'pronunciationPractice';
            //     } else if (!!this.state.multipleChoiceQuestions.length) {
            //         qPopped = this.state.multipleChoiceQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped.$key);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped.$key);
            //         this.state.currentQuestionType = 'multipleChoice';
            //     } else {
            //         qPopped = this.state.writingPracticeQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped);
            //         this.state.currentQuestionType = 'writingPractice';
            //     }
            // } else {
            //     if (!!this.state.multipleChoiceQuestions.length) {
            //         qPopped = this.state.multipleChoiceQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped.$key);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped.$key);
            //         this.state.currentQuestionType = 'multipleChoice';
            //     } else if (!!this.state.writingPracticeQuestions.length) {
            //         qPopped = this.state.writingPracticeQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped);
            //         this.state.currentQuestionType = 'writingPractice';
            //     } else {
            //         qPopped = this.state.pronunciationPracticeQuestions.pop();
            //         this.sharedDataService.set('nextSoundKey', qPopped.$key);
            //         this.soundsService.cacheSoundKey('nextSound', qPopped.$key);
            //         this.state.currentQuestionType = 'pronunciationPractice';
            //     }
            // }

            this.nextQuestion = qPopped; // will be picked up after navigation.
            console.log('Current state:', this.state);
            // make sure navService can get sound ID for params from next question. Still needed?
            return this.navService.getQuestionRoutePath(this.state.nextQuestionType, this.soundsService.getCachedSoundKey('nextSound'));
        } else {
            return null;
        }
    }

    doQuestionsRemain() {
        return !!this.state.multipleChoiceQuestions.length ||
            !!this.state.writingPracticeQuestions.length ||
            !!this.state.pronunciationPracticeQuestions.length;
    }

    updateCurrentQuestion() {
        this.currentQuestion = this.nextQuestion;
        this.state.currentQuestionType = this.state.nextQuestionType;
        this.nextQuestion = null;

        return this;
    }

    progressState() {
        this.updateCurrentQuestion();
        this.currentSound = _.find(this.sounds, sound => sound.$key === this.soundsService.getCachedSoundKey('nextSound'));

        console.log('Current Q is now:', this.currentQuestion);
        console.log('Current Q type is now:', this.state.currentQuestionType);

        return {
            nextQuestionPath: this.getSetNextQuestion(),
            sound: this.currentSound
        };
    }

    addQuestionBackToQueue() {
        this.state[`${this.state.currentQuestionType}Questions`].push(this.currentQuestion);
        console.log('Appended failed question. Current state:', this.state[`${this.state.currentQuestionType}Questions`]);

        if (!!this.nextQuestion) {
            this.state[`${this.state.nextQuestionType}Questions`].push(this.nextQuestion);
            console.log('Appended what was the next question. Current state:', this.state[`${this.state.nextQuestionType}Questions`]);
        }
    }

}

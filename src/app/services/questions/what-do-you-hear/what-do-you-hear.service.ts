import { Injectable } from '@angular/core';

@Injectable()
export class WhatDoYouHearService {
    /*
        isAnswerCorrect

        Return whether or not the user's given answer matches the correct answer.
        @param userAnswer - the user's answer.
        @param phrase - a Phrase object.

        @return boolean.
    */
    // isAnswerCorrect(userAnswer, phrase): boolean {
    //     return userAnswer.trim() === phrase.writtenForm.correct;
    // }

    /*
        isAnswerCorrect

        Return whether or not the user's given answer matches the correct answer.
        @param userAnswer - the user's answer.
        @param qObj - a Phrase or Sound object.
        @param qType - a string denoting the type of question object to be checked.

        @return boolean.
    */
    isAnswerCorrect(userAnswer, qObj, qType): boolean {
        return userAnswer.trim() === qObj.writtenForm;
    }

    /*
        isPhraseAnswerAccepted

        Return whether or not the user's given answer counts as an accepted answer.
        @param userAnswer - the user's answer.
        @param phrase - a Phrase object.

        @return boolean.
    */
    isPhraseAnswerAccepted(userAnswer, phrase): boolean {
        return phrase.acceptedForms.includes(userAnswer);
    }

    isPhraseAnswerCorrect(userAnswer, phrase): boolean {
        return this.isAnswerCorrect(userAnswer, phrase, 'phrase');
    }

    isSoundAnswerCorrect(userAnswer, sound): boolean {
        return this.isAnswerCorrect(userAnswer, sound, 'sound');
    }

}

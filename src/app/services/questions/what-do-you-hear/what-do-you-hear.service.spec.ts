import { TestBed, inject } from '@angular/core/testing';

import { WhatDoYouHearService } from './what-do-you-hear.service';

describe('WhatDoYouHearService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WhatDoYouHearService]
    });
  });

  it('should be created', inject([WhatDoYouHearService], (service: WhatDoYouHearService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { Pronunciation } from '../../../types/pronunciation';
import { AngularFireDatabase } from 'angularfire2/database';
import * as _ from 'lodash';

@Injectable()
export class PronunciationsService {

    constructor(private db: AngularFireDatabase) { }

    getPronunciationsBySoundKey(soundKey, callback) {
        const ref = this.db.database.ref('/pronunciations');

        ref.orderByKey().equalTo(soundKey).once('child_added', snapshot => {
            const key = snapshot.key;
            const pronunciation = snapshot.val();

            callback(new Pronunciation(
                key,
                pronunciation.correct,
                pronunciation.incorrect
            ));
        });
    }

    getPronunciationsBySoundKeys(soundKeys, callback) {
        const keys = (!_.isArray(soundKeys)) ? [soundKeys] : soundKeys;
        const pronunciations: Pronunciation[] = [];

        _.forEach(keys, key => {
            this.getPronunciationsBySoundKey(key, pronunciation => {
                pronunciations.push(pronunciation);

                if (pronunciations.length === soundKeys.length) {
                    callback(pronunciations);
                }
            });
        });
    }

    isAnswerCorrect(userAnswer: string, pronObj: Pronunciation) {
        return !!pronObj.correct[userAnswer];
    }

}

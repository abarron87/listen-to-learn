import { TestBed, inject } from '@angular/core/testing';

import { PronunciationsService } from './pronunciations.service';

describe('PronunciationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PronunciationsService]
    });
  });

  it('should be created', inject([PronunciationsService], (service: PronunciationsService) => {
    expect(service).toBeTruthy();
  }));
});

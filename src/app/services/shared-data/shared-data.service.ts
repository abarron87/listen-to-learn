import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class SharedDataService {
    data: any;

  constructor() {
      this.data = {};
  }

  set(childRef, payload) {
    this.data[childRef] = payload;
  }

  get(childRef) {
      return this.data[childRef] ? this.data[childRef] : null;
  }

}

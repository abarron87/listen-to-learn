import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlightSubstring'
})
export class HighlightSubstringPipe implements PipeTransform {

  transform(value: string, substr: string): string {
    // normalize the string.
    // find each instance of the substr.
    // remember the initial state of each instance.
    // for each instance wrap it in a span.

    let regex = new RegExp(substr, 'ig');
    let tokens = value.split(regex);
    let matches = value.match(regex);

    let highlighted = '';

    tokens.forEach(function(token, index) {
        highlighted += token;

        /*
         * If the token is the first or last element, split
         * will put in an empty string, so the matches array will not be the same length.
         * E.g. string "Yea he's off his tits." substr "yea he's".
         * tokens = ["", " off his tits"];
         * matches = ["Yea he's"];
         */
        if (matches[index]) {
            highlighted += '<span>' + matches[index] + '</span>';
        }
    });

    return highlighted;
  }

}

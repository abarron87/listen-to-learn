import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'quotify'
})
export class QuotifyPipe implements PipeTransform {

  transform(value: string): string {
    return '"' + value + '"';
  }

}

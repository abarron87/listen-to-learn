import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhraseRevealedComponent } from './phrase-revealed.component';

describe('PhraseRevealedComponent', () => {
  let component: PhraseRevealedComponent;
  let fixture: ComponentFixture<PhraseRevealedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhraseRevealedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhraseRevealedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

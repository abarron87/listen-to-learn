import { Component, OnInit } from '@angular/core';
import { PhrasesService } from '../../services/phrases/phrases.service';
import { SoundsService } from '../../sounds.service';
import { Phrase } from '../../types/phrase';
import { Sound } from '../../types/sound';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-phrase-revealed',
  templateUrl: './phrase-revealed.component.html',
  styleUrls: ['./phrase-revealed.component.scss'],
  providers: [PhrasesService, SoundsService]
})
export class PhraseRevealedComponent implements OnInit {
  phraseKey: string;
  phrase: Phrase;
  firstSound: Sound;

  constructor(private phrasesService: PhrasesService, private soundsService: SoundsService, private route: ActivatedRoute) {
    this.phraseKey = this.route.snapshot.paramMap.get('phraseId');
  }

  ngOnInit() {
      this.phrasesService.getPhraseByKey(this.phraseKey, phrase => {
          this.phrase = phrase;

            // load up the first sound in order to navigate to it.
            this.soundsService.getFirstSoundInPhrase(this.phrase.$key, sound => {
                this.firstSound = sound;
            });
      });
  }

}

import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { AppData } from '../../constants';

@Component({
  selector: 'app-play-video',
  templateUrl: './play-video.component.html',
  styleUrls: ['./play-video.component.scss']
})
export class PlayVideoComponent implements OnInit, AfterViewInit {
  @Input() videoFile: string;
  @Input() playSlower: boolean;
  @ViewChild('video') video: any;
  videoEl: any;
  isPlaying: boolean;
  defaultPlayIcon: string;
  defaultSlowMoPlayIcon: string;
  mainPlayButtonIcon: string;
  slowMoPlayButtonIcon: string;
  defaultSlowMoSpeed: number;
  currentSelected: string;
  hasEnded: boolean;

  constructor(private elRef: ElementRef) {
    this.defaultPlayIcon = AppData.ICONS.play;
    this.defaultSlowMoPlayIcon = AppData.ICONS.slowMoPlay;
    this.defaultSlowMoSpeed = AppData.SLOW_MO_SPEED;
  }

  ngOnInit() {
      this.videoEl = this.elRef.nativeElement.querySelector('video');
      this.mainPlayButtonIcon = this.defaultPlayIcon;
      this.slowMoPlayButtonIcon = this.defaultSlowMoPlayIcon;
      this.hasEnded = true;
  }

  ngAfterViewInit() {

  }

  togglePlayback() {
      if (this.videoEl.paused) {
        this.isPlaying = true; // better here than on playing event handler.
        this.videoEl.play();
      } else {
        this.isPlaying = false; // better here than on paused event handler.
        this.videoEl.pause();
      }

      this.hasEnded = false;
  }

  togglePlaybackNormalSpeed() {
    this.videoEl.playbackRate = 1;
    this.currentSelected = 'normal';
    this.togglePlayback();
  }

  togglePlaybackSlowMo() {
      this.videoEl.playbackRate = this.defaultSlowMoSpeed;
      this.currentSelected = 'slow';
      this.togglePlayback();
  }

  onVideoPlaying() {
      console.log('playing');
      this.mainPlayButtonIcon = AppData.ICONS.pause;
      this.slowMoPlayButtonIcon = AppData.ICONS.pause;
  }

  onVideoPaused() {
      this.mainPlayButtonIcon = this.defaultPlayIcon;
      this.slowMoPlayButtonIcon = this.defaultSlowMoPlayIcon;
  }

  resetVideo() {
      this.mainPlayButtonIcon = this.defaultPlayIcon;
      this.slowMoPlayButtonIcon = this.defaultSlowMoPlayIcon;
      this.currentSelected = 'none';
      this.isPlaying = false;
  }

}

import { Component, OnInit } from '@angular/core';
import { PhrasesService } from '../../services/phrases/phrases.service';
import { Phrase } from '../../types/phrase';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-phrase-intro',
  templateUrl: './phrase-intro.component.html',
  styleUrls: ['./phrase-intro.component.scss'],
  providers: [PhrasesService]
})
export class PhraseIntroComponent implements OnInit {
  phraseKey: string;
  phrase: Phrase;

  constructor(private phrasesService: PhrasesService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.phraseKey = this.route.snapshot.paramMap.get('phraseId');

    this.phrasesService.getPhraseByKey(this.phraseKey, phrase => {
        this.phrase = phrase;
    });
  }

}

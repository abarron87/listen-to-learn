import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhraseIntroComponent } from './phrase-intro.component';

describe('PhraseIntroComponent', () => {
  let component: PhraseIntroComponent;
  let fixture: ComponentFixture<PhraseIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhraseIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhraseIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

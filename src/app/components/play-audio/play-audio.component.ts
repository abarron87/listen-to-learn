import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-play-audio',
  templateUrl: './play-audio.component.html',
  styleUrls: ['./play-audio.component.scss']
})
export class PlayAudioComponent implements OnInit {

  @Input() audioTrack: string;
  audioButtonIcon: string;
  progOverlayStartPos: string;
  progOverlayPos: string;
  soundEl: any;

  constructor(private elRef: ElementRef) { }

  ngOnInit() {
      this.audioButtonIcon = 'volume_up'; // pipe?
      this.soundEl = this.elRef.nativeElement.querySelector('.sound');
  }

  playSound() {
    let progOverlayEl = this.elRef.nativeElement.querySelector('.audio-button .progress-overlay');
    console.log(this.elRef.nativeElement);

    // hacky non-angular way of getting CSS 'left' prop value.
    // https://blog.angularindepth.com/exploring-angular-dom-abstractions-80b3ebcfc02
    if (window && window.getComputedStyle) {
        this.progOverlayStartPos = window.getComputedStyle(progOverlayEl).getPropertyValue('left');
    }

    this.soundEl.play();
  }

    onAudioPlaying() {
        this.audioButtonIcon = 'pause';
        console.log('playing');
    }

    onAudioReady() {
        this.audioButtonIcon = 'volume_up';
        console.log('ready');
    }

    onAudioEnded() {
        this.progOverlayPos = this.progOverlayStartPos;
        console.log('ended');
    }

    /*
    * Get the audio element.
    * Calculate the percentage of the audio that has played.
    * Convert the progress "bar's" left position to a positive integer.
    * Calculate the progress as a percentage of the left position.
    * The 'left' property is incremented according to the percentage.
    * https://stackoverflow.com/questions/27894056/update-progress-bar-using-percentage-values
    */
    onAudioProgress(event) {
        let target = event.target || event.srcElement || event.currentTarget;
        let percentComplete = Math.floor((100 / target.duration) * target.currentTime);
        let startPos = Math.abs(parseInt(this.progOverlayStartPos));
        let percentOfEl = (startPos * (percentComplete / 100));

        this.progOverlayPos = parseInt(this.progOverlayStartPos) + percentOfEl + 'px';

        // take left property
        // make it a number
        // make it positive
        // get percentage of that
        // add that to the position
    }

}

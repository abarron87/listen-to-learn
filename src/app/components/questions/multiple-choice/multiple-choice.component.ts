import { Component, OnInit, OnDestroy } from '@angular/core';
import { SharedDataService } from '../../../services/shared-data/shared-data.service';
import { Phrase } from '../../../types/phrase';
import { Sound } from '../../../types/sound';
import { PhrasesService } from '../../../services/phrases/phrases.service';
import { MultipleChoiceService } from '../../../services/questions/multiple-choice/multiple-choice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MultipleChoice } from '../../../types/multiple-choice';
import * as _ from 'lodash';
import { TestQuestionsService } from '../../../services/questions/test-questions/test-questions.service';
import { MultipleChoiceAnswer } from '../../../types/multiple-choice-answer';
import { SoundsService } from '../../../sounds.service';
import { TestQuestionsState } from '../../../types/test-questions-state';
import { Subscription } from 'rxjs/Subscription';
import { NavService } from '../../../services/nav/nav.service';

@Component({
    selector: 'app-multiple-choice',
    templateUrl: './multiple-choice.component.html',
    styleUrls: ['./multiple-choice.component.scss']
})
export class MultipleChoiceComponent implements OnInit, OnDestroy {
    phrase: Phrase;
    sound: Sound;
    soundKey: string;
    choiceObj: MultipleChoice;
    choiceObjKeys: string[];
    selectedChoice: string;
    choices: MultipleChoiceAnswer[];
    nextQuestionPath: any;
    userAnswer: string;
    answerValidity: string;
    correctAnswer: object;
    objectKeys = Object.keys;
    state: TestQuestionsState;
    serviceReady: boolean;
    readySubscription: Subscription;

    constructor(
        private sharedDataService: SharedDataService, private phrasesService: PhrasesService,
        private route: ActivatedRoute, private multipleChoiceService: MultipleChoiceService,
        private testQuestionsService: TestQuestionsService, private soundsService: SoundsService,
        private router: Router, private navService: NavService
    ) { }

    ngOnInit() {

        //TODO: use subscription for this.
        // this.soundKey = this.route.snapshot.paramMap.get('soundId');
        this.route.paramMap.subscribe(paramMap => {
            this.answerValidity = null;
            this.choices = null;

            this.soundKey = paramMap.get('soundId');

            if (this.testQuestionsService.ready) {
                // move next question to current question in service.
                this.nextQuestionPath = this.testQuestionsService.progressState().nextQuestionPath;
                console.log('Current Q updated. Next q will be:', this.nextQuestionPath);

                this.finishInit();
            } else {
                this.readySubscription = this.testQuestionsService.ready$.subscribe(ready => {
                    this.nextQuestionPath = this.testQuestionsService.progressState().nextQuestionPath;
                    console.log('Current Q updated. $Next q will be:', this.nextQuestionPath);
                    this.finishInit();
                });
            }
        });



        // this.testQuestionsService.initState(this.route.snapshot.paramMap.get('phraseId'));



        // this.testQuestionsService.getNextQuestion();
    }

    finishInit() {
        this.sound = _.find(this.testQuestionsService.sounds, sound => {
            return sound.$key === this.testQuestionsService.currentQuestion.$key;
        });
        console.log('MC - this.sound:', this.sound);

        this.choiceObj = this.testQuestionsService.currentQuestion;

        /*
            If coming through the app screens sequentially, the Phrase will be cached.
            If going direct to the URL, will need to retrieve it from the DB
        */
        const cachedPhrase = this.sharedDataService.get('currentPhrase');
        if (!!cachedPhrase) {
            this.phrase = cachedPhrase;
        } else {
            this.phrasesService.getPhraseByKey(this.route.snapshot.paramMap.get('phraseId'), phrase => this.phrase = phrase);
            this.soundsService.init(this.route.snapshot.paramMap.get('phraseId')); // remove
        }

        // this.soundsService.getSoundByKey(this.soundKey, sound => this.sound = sound);
        // this.state = this.testQuestionsService.state;


        console.log('this.choiceObj', this.choiceObj);

        // this.multipleChoiceService.getChoicesBySoundKey(this.soundKey, choiceObj => {
            // this.choiceObj = choiceObj; // data in preferred state on the server { correct: [], incorrect: [] }.

            // get array of strings containing the keys of the incorrect choices. Randomise them.
            this.choiceObjKeys = _.shuffle(this.objectKeys(this.choiceObj.incorrect).concat(this.objectKeys(this.choiceObj.correct)));

            // create array of objects. Don't put it in this.choices right away or the view will update.
            const temp: MultipleChoiceAnswer[] = [];

            // add each incorrect answer as an object in the temp array.
            // 'key' here is the value of each array item. 'key' refers to how it is used to look up the value later.
            // this.choiceObjKeys === ['key3', 'key1', 'key4', 'key2'];

            console.log('shuffled', this.choiceObjKeys);
            _.forEach(this.choiceObjKeys, key => {
                const obj = new MultipleChoiceAnswer(
                    key,
                    // if it's not in incorrect, it'll be the correct answer.
                    this.choiceObj.incorrect[key] || this.choiceObj.correct[key]
                );

                // lookup object with current key i.e. 'key2'.
                // create properties for key and value. This way, key can easily be used in the view.
                temp.push(obj);
            });
            // array will contain new structure: [{ 'key1': 'value1' }, { 'key2': 'value2' }, { 'key3': 'value3' }, { 'key4': 'value4' }]

            // console.log('choiceObj', this.choiceObj.incorrect);
            this.choices = temp;
            console.log('choices', this.choices);
        // });
    }

    checkAnswer() {
        console.log('Checking answer is ' + this.userAnswer);
        const self = this;

        this.correctAnswer = _.find(this.choices, answer => {
            return answer.key === this.userAnswer;
        });

        if (this.multipleChoiceService.isAnswerCorrect(this.userAnswer, this.choiceObj)) {
            this.answerValidity = 'correct';
            // console.log(this.correctAnswer);
        } else {
            this.answerValidity = 'invalid';
        }
    }

    continue() {
        if (this.nextQuestionPath) {
            this.router.navigate(this.nextQuestionPath);
        } else {
            prompt('This is the final question');
        }
    }

    goToReview() {
        console.log('MCComponent - Adding question to queue.');
        this.testQuestionsService.addQuestionBackToQueue();
        console.log('Navigating to the study page...');
        this.router.navigate(this.navService.getStudyRoutePath(this.soundKey), { queryParams: { 'review': true } });
    }

    // useful to know but not currently used.
    // transformKey(key) {
    //     return [this.choices[key]].map(_.values);
    // }

    ngOnDestroy() {
        if (this.readySubscription) {
            this.readySubscription.unsubscribe();
        }
        console.log('destroying');
    }

}

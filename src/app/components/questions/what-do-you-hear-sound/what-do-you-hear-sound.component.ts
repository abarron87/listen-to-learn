import { Component, OnInit, OnDestroy } from '@angular/core';
import { Phrase } from '../../../types/phrase';
import { Sound } from '../../../types/sound';
import { AppData } from '../../../constants';
import { SharedDataService } from '../../../services/shared-data/shared-data.service';
import { WhatDoYouHearService } from '../../../services/questions/what-do-you-hear/what-do-you-hear.service';
import { PhrasesService } from '../../../services/phrases/phrases.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SoundsService } from '../../../sounds.service';
import { Subscription } from 'rxjs/Subscription';
import { TestQuestionsService } from '../../../services/questions/test-questions/test-questions.service';
import { TestQuestionsState } from '../../../types/test-questions-state';
import * as _ from 'lodash';
import { NavService } from '../../../services/nav/nav.service';

@Component({
    selector: 'app-what-do-you-hear-sound',
    templateUrl: './what-do-you-hear-sound.component.html',
    styleUrls: ['./what-do-you-hear-sound.component.scss']
})
export class WhatDoYouHearSoundComponent implements OnInit, OnDestroy {
    phraseId: string;
    soundId: string;
    questionTitle: string;
    phrase: Phrase;
    sound: Sound;
    userAnswer: string;
    answerValidity: string;
    readySubscription: Subscription;
    nextQuestionPath: any;
    state: TestQuestionsState;

    constructor(
        private phrasesService: PhrasesService, private route: ActivatedRoute,
        private whatDoYouHearService: WhatDoYouHearService, private sharedDataService: SharedDataService,
        private soundsService: SoundsService, private testQuestionsService: TestQuestionsService,
        private router: Router, private navService: NavService
    ) {
        this.phraseId = this.route.snapshot.paramMap.get('phraseId');
        this.soundId = this.route.snapshot.paramMap.get('soundId');
        this.userAnswer = '';
        this.answerValidity = '';
    }

    ngOnInit() {
        this.questionTitle = AppData.QUESTIONS.whatDoYouHearSound;
        // this.state = this.testQuestionsService.state;
        this.route.paramMap.subscribe(paramMap => {
            this.userAnswer = null;
            this.answerValidity = null;
            this.nextQuestionPath = null;

            if (this.testQuestionsService.ready) {
                // move next question to current question in service.
                this.nextQuestionPath = this.testQuestionsService.progressState().nextQuestionPath;
                console.log('Current Q updated. Next q will be:', this.nextQuestionPath);
                this.finishInit();
            } else {
                this.readySubscription = this.testQuestionsService.ready$.subscribe(ready => {
                    this.nextQuestionPath = this.testQuestionsService.progressState().nextQuestionPath;
                    console.log('Current Q updated. $Next q will be:', this.nextQuestionPath);
                    this.finishInit();
                });
            }

            // this.soundKey = paramMap.get('soundId');
        });
    }

    finishInit() {
        this.sound = _.find(this.testQuestionsService.sounds, sound => {
            return sound.$key === this.testQuestionsService.currentQuestion;
        });

        console.log('WDYH - this.sound:', this.sound);
        /*
            If coming through the app screens sequentially, the Phrase will be cached.
            If going direct to the URL, will need to retrieve it from the DB
        */
        const cachedPhrase = this.sharedDataService.get('currentPhrase');
        if (!!cachedPhrase) {
            this.phrase = cachedPhrase;
        } else {
            this.phrasesService.getPhraseByKey(this.phraseId, phrase => this.phrase = phrase);
        }

        // this.soundsService.getSoundByKey(this.soundId, sound => this.sound = sound);

        console.log('Current question ID:', this.testQuestionsService.currentQuestion);
    }

    checkAnswer() {
        console.log('Checking answer is ' + this.userAnswer);

        if (this.whatDoYouHearService.isSoundAnswerCorrect(this.userAnswer, this.sound)) {
            this.answerValidity = 'correct';
        } else {
            this.answerValidity = 'invalid';
        }
    }

    onKeyUp(event) {
        if (event.keyCode === 13) {
            this.checkAnswer();
        }
    }

    /*
        1. Gets question wrong.
        2. Button appears to review.
        3. Current question is put back in the service's array for the failed question type.
        4. Goes to the sound-to-study page with a query param of "review".
        5. In the background, progressState is called.
        6. After reviewing, user clicks button and returns to the test section.
    */

    continue() {
        if (this.nextQuestionPath) {
            console.log('Going to next page.');
            this.router.navigate(this.nextQuestionPath);
        } else {
            prompt('This is the final question');
        }
    }

    goToReview() {
        console.log('MCComponent - Adding question to queue.');
        this.testQuestionsService.addQuestionBackToQueue();
        console.log('Navigating to the study page...');
        this.router.navigate(this.navService.getStudyRoutePath(this.sound.$key), { queryParams: { 'review': true } });
    }

    ngOnDestroy() {
        if (this.readySubscription) {
            this.readySubscription.unsubscribe();
        }
        // prompt('destroying');
        console.log('destroying');
    }

}

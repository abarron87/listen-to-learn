import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatDoYouHearSoundComponent } from './what-do-you-hear-sound.component';

describe('WhatDoYouHearSoundComponent', () => {
  let component: WhatDoYouHearSoundComponent;
  let fixture: ComponentFixture<WhatDoYouHearSoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatDoYouHearSoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatDoYouHearSoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatDoYouHearComponent } from './what-do-you-hear.component';

describe('WhatDoYouHearComponent', () => {
  let component: WhatDoYouHearComponent;
  let fixture: ComponentFixture<WhatDoYouHearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatDoYouHearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatDoYouHearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

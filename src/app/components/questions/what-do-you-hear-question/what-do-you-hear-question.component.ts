import { Component, OnInit, OnDestroy } from '@angular/core';
import { Phrase } from '../../../types/phrase';
import { AppData } from '../../../constants';
import { WhatDoYouHearService } from '../../../services/questions/what-do-you-hear/what-do-you-hear.service';
import { PhrasesService } from '../../../services/phrases/phrases.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { TestQuestionsService } from '../../../services/questions/test-questions/test-questions.service';

@Component({
  selector: 'app-what-do-you-hear-question',
  templateUrl: './what-do-you-hear-question.component.html',
  styleUrls: ['./what-do-you-hear-question.component.scss'],
  providers: [
      PhrasesService,
      WhatDoYouHearService
  ]
})
export class WhatDoYouHearQuestionComponent implements OnInit {
    phraseId: string;
    question: string;
    phrase: Phrase;
    userAnswer: string;
    answerValidity: string;

    constructor(
        private phrasesService: PhrasesService, private route: ActivatedRoute,
        private whatDoYouHearService: WhatDoYouHearService, private router: Router,
        private testQuestionsService: TestQuestionsService
    ) {
        this.phraseId = this.route.snapshot.paramMap.get('phraseId');
        this.userAnswer = '';
        this.answerValidity = '';
    }

    ngOnInit() {
        this.question = AppData.QUESTIONS.whatDoYouHear;

        this.phrasesService.getPhraseByKey(this.phraseId, phrase => {
            this.phrase = phrase;
        });
    }

    checkAnswer() {
        console.log('Checking answer is ' + this.userAnswer);

        if (this.whatDoYouHearService.isPhraseAnswerCorrect(this.userAnswer, this.phrase)) {
            this.answerValidity = 'correct';
        } else if (this.whatDoYouHearService.isPhraseAnswerAccepted(this.userAnswer, this.phrase)) {
            this.answerValidity = 'accepted';
        } else {
            this.answerValidity = 'invalid';
        }
    }

    onKeyUp(event) {
        if (event.keyCode === 13) {
            this.checkAnswer();
        }
    }

}

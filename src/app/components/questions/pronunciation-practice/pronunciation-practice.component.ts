import { Component, OnInit, OnDestroy } from '@angular/core';
import { Phrase } from '../../../types/phrase';
import { Sound } from '../../../types/sound';
import { Pronunciation } from '../../../types/pronunciation';
import { PronunciationAnswer } from '../../../types/pronunciation-answer';
import { PhrasesService } from '../../../services/phrases/phrases.service';
import { SharedDataService } from '../../../services/shared-data/shared-data.service';
import { TestQuestionsService } from '../../../services/questions/test-questions/test-questions.service';
import { SoundsService } from '../../../sounds.service';
import { PronunciationsService } from '../../../services/questions/pronunciations/pronunciations.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Subscription';
import { NavService } from '../../../services/nav/nav.service';

@Component({
    selector: 'app-pronunciation-practice',
    templateUrl: './pronunciation-practice.component.html',
    styleUrls: ['./pronunciation-practice.component.scss']
})
export class PronunciationPracticeComponent implements OnInit, OnDestroy {
    phrase: Phrase;
    sound: Sound;
    soundKey: string;
    pronObj: Pronunciation;
    pronObjKeys: string[];
    selectedChoice: string;
    choices: PronunciationAnswer[];
    nextQuestionPath: any;
    userAnswer: string;
    answerValidity: string;
    correctAnswer: object;
    objectKeys = Object.keys;
    readySubscription: Subscription;

    constructor(
        private sharedDataService: SharedDataService, private phrasesService: PhrasesService,
        private route: ActivatedRoute, private pronunciationsService: PronunciationsService,
        private testQuestionsService: TestQuestionsService, private soundsService: SoundsService,
        private router: Router, private navService: NavService
    ) { }

    ngOnInit() {
        // this.soundKey = this.route.snapshot.paramMap.get('soundId');

        this.route.paramMap.subscribe(paramMap => {
            this.answerValidity = null;
            this.choices = null;

            if (this.testQuestionsService.ready) {
                // move next question to current question in service.
                const latestStateData = this.testQuestionsService.progressState();
                this.nextQuestionPath = latestStateData.nextQuestionPath;
                this.sound = latestStateData.sound;
                // this.nextQuestionPath = this.testQuestionsService.progressState();
                console.log('Current Q updated. Next q will be:', this.nextQuestionPath);
                this.finishInit();
            } else {
                this.readySubscription = this.testQuestionsService.ready$.subscribe(ready => {
                    const latestStateData = this.testQuestionsService.progressState();
                    this.nextQuestionPath = latestStateData.nextQuestionPath;
                    this.sound = latestStateData.sound;
                    console.log('this.sound', this.sound);
                    console.log('Current Q updated. $Next q will be:', this.nextQuestionPath);
                    this.finishInit();
                });

                // this.testQuestionsService.initState(this.route.snapshot.paramMap.get('phraseId'));
            }

            this.soundKey = paramMap.get('soundId');
        });
    }

    finishInit() {
        // this.readySubscription = this.testQuestionsService.ready$.subscribe(ready => {
        //     this.serviceReady = ready;
        //     this.nextQuestion = this.testQuestionsService.getNextQuestion();
        // });
        this.sound = _.find(this.testQuestionsService.sounds, sound => {
            return sound.$key === this.testQuestionsService.currentQuestion.$key;
        });
        console.log('PP - this.sound:', this.sound);

        this.pronObj = this.testQuestionsService.currentQuestion;
        /*
            If coming through the app screens sequentially, the Phrase will be cached.
            If going direct to the URL, will need to retrieve it from the DB
        */

        const cachedPhrase = this.sharedDataService.get('currentPhrase');

        if (!!cachedPhrase) {
            this.phrase = cachedPhrase;
        } else {
            this.phrasesService.getPhraseByKey(this.route.snapshot.paramMap.get('phraseId'), phrase => {
                this.phrase = phrase;
                this.sharedDataService.set('currentPhrase', phrase);
            });
            // this.soundsService.init(this.route.snapshot.paramMap.get('phraseId')); // remove
        }


        // this.pronObj = this.testQuestionsService.currentQuestion;
        // console.log('this.pronObj', this.pronObj);

        // this.pronunciationsService.getPronunciationsBySoundKey(this.soundKey, pronObj => {
            // this.pronObj = pronObj; // data in preferred state on the server { correct: [], incorrect: [] }.

            // get array of strings containing the keys of the incorrect choices. Randomise them.
            this.pronObjKeys = _.shuffle(this.objectKeys(this.pronObj.incorrect).concat(this.objectKeys(this.pronObj.correct)));

            // create array of objects. Don't put it in this.choices right away or the view will update.
            const temp: PronunciationAnswer[] = [];

            // add each incorrect answer as an object in the temp array.
            // 'key' here is the value of each array item. 'key' refers to how it is used to look up the value later.
            // this.choiceObjKeys === ['key3', 'key1', 'key4', 'key2'];

            console.log('shuffled', this.pronObjKeys);
            _.forEach(this.pronObjKeys, key => {
                const obj = new PronunciationAnswer(
                    key,
                    // if it's not in incorrect, it'll be the correct answer.
                    this.pronObj.incorrect[key] || this.pronObj.correct[key]
                );

                // lookup object with current key i.e. 'key2'.
                // create properties for key and value. This way, key can easily be used in the view.
                temp.push(obj);
            });
            // array will contain new structure: [{ 'key1': 'value1' }, { 'key2': 'value2' }, { 'key3': 'value3' }, { 'key4': 'value4' }]

            this.choices = temp;
            console.log('choices', this.choices);
        // });
    }

    checkAnswer() {
        console.log('Checking answer is ' + this.userAnswer);
        const self = this;
        if (this.pronunciationsService.isAnswerCorrect(this.userAnswer, this.pronObj)) {
            this.answerValidity = 'correct';

            this.correctAnswer = _.find(this.choices, function (answer) {
                return answer.key === self.userAnswer;
            });

            // console.log(this.correctAnswer);
        } else {
            this.answerValidity = 'invalid';
        }
    }

    selectPronunciation(key) {
        this.userAnswer = key;
        console.log('Pronunciation selected: ', key);
    }

    continue() {
        if (this.nextQuestionPath) {
            this.router.navigate(this.nextQuestionPath);
        } else {
            prompt('This is the final question');
        }
    }

    goToReview() {
        console.log('MCComponent - Adding question to queue.');
        this.testQuestionsService.addQuestionBackToQueue();
        console.log('Navigating to the study page...');
        this.router.navigate(this.navService.getStudyRoutePath(this.soundKey), { queryParams: { 'review': true } });
    }

    ngOnDestroy() {
        if (this.readySubscription) {
            this.readySubscription.unsubscribe();
        }
        console.log('destroying');
    }

}

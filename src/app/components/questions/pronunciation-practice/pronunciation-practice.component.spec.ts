import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PronunciationPracticeComponent } from './pronunciation-practice.component';

describe('PronunciationPracticeComponent', () => {
  let component: PronunciationPracticeComponent;
  let fixture: ComponentFixture<PronunciationPracticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PronunciationPracticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PronunciationPracticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Phrase } from '../../../types/phrase';
import { AppData } from '../../../constants';
// import { QuestionsService } from '../../../services/questions/questions.service';
import { HowManyWordsService } from '../../../services/questions/how-many-words/how-many-words.service';
import { PhrasesService } from '../../../services/phrases/phrases.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-how-many-words-question',
    templateUrl: './how-many-words-question.component.html',
    styleUrls: ['./how-many-words-question.component.scss'],
    providers: [
        PhrasesService,
        HowManyWordsService
    ]
})
export class HowManyWordsQuestionComponent implements OnInit {
    phraseId: string;
    question: string;
    phrase: Phrase;
    userAnswer: string;
    answerValidity: string;

    constructor(private phrasesService: PhrasesService, private route: ActivatedRoute, private howManyWordsService: HowManyWordsService) {
        this.phraseId = this.route.snapshot.paramMap.get('phraseId');
        this.userAnswer = '';
        this.answerValidity = '';
    }

    ngOnInit() {
        this.question = AppData.QUESTIONS.howMany;

        this.phrasesService.getPhraseByKey(this.phraseId, phrase => {
            this.phrase = phrase;
        });
    }

    checkAnswer() {
        console.log('Checking answer is ' + this.userAnswer);

        if (this.howManyWordsService.isAnswerCorrect(this.userAnswer, this.phrase)) {
            this.answerValidity = 'correct';
        } else if (this.howManyWordsService.isAnswerAccepted(this.userAnswer, this.phrase)) {
            this.answerValidity = 'accepted';
        } else {
            this.answerValidity = 'invalid';
        }
    }

    onKeyUp(event) {
        if (event.keyCode === 13) {
            this.checkAnswer();
        }
    }

}

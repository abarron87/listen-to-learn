import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
    MatButtonModule, MatCheckboxModule, MatCardModule, MatListModule,
    MatFormFieldModule, MatInputModule, MatRadioModule
} from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';

import { SoundsService } from './sounds.service';

import { SoundsComponent } from './sounds/sounds.component';
import { SoundToStudyComponent } from './sound-to-study/sound-to-study.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { HighlightSubstringPipe } from './pipes/highlight-substring.pipe';
import { QuotifyPipe } from './pipes/quotify.pipe';
import { HowManyWordsQuestionComponent } from './components/questions/how-many-words-question/how-many-words-question.component';
import { PhraseRevealedComponent } from './components/phrase-revealed/phrase-revealed.component';
import { PlayAudioComponent } from './components/play-audio/play-audio.component';
import { PlayVideoComponent } from './components/play-video/play-video.component';
import { WhatDoYouHearQuestionComponent } from './components/questions/what-do-you-hear-question/what-do-you-hear-question.component';
import { PhraseIntroComponent } from './components/phrase-intro/phrase-intro.component';
import { MultipleChoiceComponent } from './components/questions/multiple-choice/multiple-choice.component';
import { SharedDataService } from './services/shared-data/shared-data.service';
import { WhatDoYouHearSoundComponent } from './components/questions/what-do-you-hear-sound/what-do-you-hear-sound.component';
import { MultipleChoiceService } from './services/questions/multiple-choice/multiple-choice.service';
import { WhatDoYouHearService } from './services/questions/what-do-you-hear/what-do-you-hear.service';
import { PronunciationsService } from './services/questions/pronunciations/pronunciations.service';
import { TestQuestionsService } from './services/questions/test-questions/test-questions.service';
import { PhrasesService } from './services/phrases/phrases.service';
import { PronunciationPracticeComponent } from './components/questions/pronunciation-practice/pronunciation-practice.component';
import { NavService } from './services/nav/nav.service';


@NgModule({
    declarations: [
        AppComponent,
        SoundsComponent,
        SoundToStudyComponent,
        NotFoundComponent,
        HomeComponent,
        HighlightSubstringPipe,
        QuotifyPipe,
        HowManyWordsQuestionComponent,
        PhraseRevealedComponent,
        PlayAudioComponent,
        PlayVideoComponent,
        WhatDoYouHearQuestionComponent,
        PhraseIntroComponent,
        MultipleChoiceComponent,
        WhatDoYouHearSoundComponent,
        PronunciationPracticeComponent
    ],
    imports: [
        BrowserModule,
        FlexLayoutModule,
        FormsModule,
        RouterModule.forRoot([
            {
                path: '',
                component: HomeComponent
            },
            {
                path: 'phrase-intro/:phraseId',
                component: PhraseIntroComponent
            },
            {
                path: 'questions/what-do-you-hear/:phraseId',
                component: WhatDoYouHearQuestionComponent
            },
            {
                path: 'questions/how-many-words/:phraseId',
                component: HowManyWordsQuestionComponent
            },
            {
                path: 'phrase-revealed/:phraseId',
                component: PhraseRevealedComponent
            },
            {
                path: 'sounds-to-study/:phraseId/:soundId',
                component: SoundToStudyComponent
            },
            {
                path: 'test/multiple-choice/:phraseId/:soundId',
                component: MultipleChoiceComponent
            },
            {
                path: 'test/what-do-you-hear/:phraseId/:soundId',
                component: WhatDoYouHearSoundComponent
            },
            {
                path: 'test/pronunciation-practice/:phraseId/:soundId',
                component: PronunciationPracticeComponent
            },
            {
                path: 'sounds',
                component: SoundsComponent
            },
            {
                path: '**',
                component: NotFoundComponent
            }
        ]),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatToolbarModule,
        MatCardModule,
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule
    ],
    providers: [
        SoundsService, SharedDataService,
        MultipleChoiceService, WhatDoYouHearService,
        PronunciationsService, TestQuestionsService,
        PhrasesService, NavService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-sounds',
  templateUrl: './sounds.component.html',
  styleUrls: ['./sounds.component.scss']
})
export class SoundsComponent implements OnInit {
  sounds: any[];

  constructor(db: AngularFireDatabase) {
    db.list('/sounds')
      .valueChanges()
      .subscribe(sounds => {
        this.sounds = sounds;
        console.log(this.sounds);
      });
  }

  ngOnInit() {
  }

}

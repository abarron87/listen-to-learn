/*
    Data model for a PronunciationAnswer to a Pronunciation question.
    key refers to the object key in the db for the answer.
    value refers to the object value in the db for the answer.
    e.g. "office" (key): "/path/to/my/audio/file" (value)
*/

export class PronunciationAnswer  {
    constructor(
        public key: string,
        public value: string) {
    }
}

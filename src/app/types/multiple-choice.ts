/*
    Data model for a Multiple Choice question.
*/

export class MultipleChoice  {
    constructor(
        public $key: string,
        public correct: string,
        public incorrect: string[]) {
    }
}

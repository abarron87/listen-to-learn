/*
    Data model for a MultipleChoiceAnswer to a Multiple Choice question.
*/

export class MultipleChoiceAnswer  {
    constructor(
        public key: string,
        public value: string) {
    }
}

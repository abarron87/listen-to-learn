/*
    Data model for a Pronunciation object.
*/

export class Pronunciation  {
    constructor(
        public $key: string,
        public correct: object,
        public incorrect: object) {

        }
}

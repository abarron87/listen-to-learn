/*
    Data model for a Phrase.
*/

export class Phrase  {
    constructor(
        public $key: string,
        public writtenForm: string,
        public audio: string,
        public video: string,
        public explanation: string,
        public numWords: object) {

    }
}

/*
    Data model for a Sound.
*/

export class Sound  {
    constructor(
        public $key: string,
        public writtenForm: string,
        public audio: string,
        public notes: object[],
        public speechAspects: object[]) {

    }
}

import { MultipleChoice } from './multiple-choice';
import { Pronunciation } from './pronunciation';
/*
    Data model for the state of the test questions asked after studying the sounds.
*/

export class TestQuestionsState  {
    constructor(
        public multipleChoiceQuestions: MultipleChoice[] = [],
        public writingPracticeQuestions: string[] = [],
        public pronunciationPracticeQuestions: Pronunciation[] = [],
        public currentQuestionType?: string,
        public nextQuestionType?: string) {
    }
}

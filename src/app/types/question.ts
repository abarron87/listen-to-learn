/*
    Data model for a Question.
*/

export class Question  {
    constructor(
        public route: any[] = [],
        public question?: any) {
    }
}

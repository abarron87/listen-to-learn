import { Injectable, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import { Phrase } from './types/phrase';
import { Sound } from './types/sound';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { SharedDataService } from './services/shared-data/shared-data.service';

@Injectable()
export class SoundsService {
    currentPosition: number;
    soundCountPerPhrase: number;

    constructor(
        private db: AngularFireDatabase, private route: ActivatedRoute,
        private sharedDataService: SharedDataService) {

    }

    /*
        init
        To be called when sounds are used in context of a parent phrase. i.e. when in a level.
    */
    init(phraseKey, callback?) {
        // get the number of sounds in a phrase.
        this.db.database.ref(`/soundsPerPhrase`)
            .orderByKey()
            .equalTo(phraseKey)
            .once('child_added',
                snapshot => {
                    this.soundCountPerPhrase = snapshot.numChildren();

                    if (!!callback && typeof callback === 'function') {
                        callback();
                    }
                }
            );
    }

    /*
        getSoundByKey

        Looks up a sound from the /sounds node by its key.
        Instantiates a Sound and sends it back to the caller by invoking the callback.
    */
    getSoundByKey(soundId, callback) {
        const soundsRef = this.db.database.ref('/sounds');

        soundsRef.orderByKey().equalTo(soundId).on('child_added', snapshot => {
            const key = snapshot.key;
            const sound = snapshot.val();

            callback(new Sound(key, sound.writtenForm, sound.audio, sound.notes, sound.speechAspects));
        });
    }

    /*
        getNextOrPreviousSoundInPhrase

        Looks up the sound references under a given phrase key in soundsPerPhrase.
        Filters the references down to 1 using a given sound key.
        Gets the order property's value of the sound reference.
        Uses the order and direction values to get the order of the next or previous sound.
        Uses the new order to look up the new sound.

        @param phraseId - the ID of the current phrase under which to find sounds.
        @param currentSoundId - the ID of the current sound. Sounds are retrieved relative to this sound.
        @param direction - 'previous' or anything else but assumed 'next'.
        @param callback - callback invoked. Usually from the component which takes the returned sound.
    */
    getNextOrPreviousSoundInPhrase(phraseId, currentSoundId, direction, callback) {
        console.log(`/soundsPerPhrase/${phraseId}`);
        const soundsPerPhraseRef = this.db.database.ref(`/soundsPerPhrase/${phraseId}`);

        soundsPerPhraseRef.orderByKey().equalTo(currentSoundId).on('child_added', snapshot => {
            const order = +snapshot.val().order;

            if (order === (direction === 'previous' ? 0 : (this.soundCountPerPhrase - 1))) {
                callback(null);
            } else {
                soundsPerPhraseRef
                    .orderByChild('order')
                    .equalTo((direction === 'previous') ? order - 1 : order + 1)
                    .on('child_added', snapshot2 => {
                        this.getSoundByKey(snapshot2.val().id, callback);
                    });
            }
        });
    }

    /*
        getPreviousSoundInPhrase

        Shortcut method to get the previous sound. The one whose order is one less than the current.
        @params - See signature for getNextOrPreviousSoundInPhrase.
    */
    getPreviousSoundInPhrase(phraseId, currentSoundId, callback) {
        this.getNextOrPreviousSoundInPhrase(phraseId, currentSoundId, 'previous', callback);
    }

    /*
        getNextSoundInPhrase

        Shortcut method to get the next sound. The one whose order is one less than the current.
        @params - See signature for getNextOrPreviousSoundInPhrase.
    */
    getNextSoundInPhrase(phraseId, currentSoundId, callback) {
        this.getNextOrPreviousSoundInPhrase(phraseId, currentSoundId, 'next', callback);
    }

    getFirstSoundInPhrase(phraseId, callback) {
        const soundsPerPhraseRef = this.db.database.ref(`/soundsPerPhrase/${phraseId}`);

        soundsPerPhraseRef.orderByChild('order').equalTo(0).once('child_added', snapshot => {
            console.log('snapshot', snapshot.val());

            const soundsRef = this.db.database.ref(`/sounds`);

            soundsRef.orderByKey().equalTo(snapshot.key).once('child_added', soundSnapshot => {
                const key = soundSnapshot.key;
                const sound = soundSnapshot.val();

                callback(new Sound(key, sound.writtenForm, sound.audio, sound.notes, sound.speechAspects));
            });
        });
    }

    getAllSoundsByPhrase(phraseId, callback) {
        // const soundsPerPhraseRef = this.db.database.ref(`/soundsPerPhrase/${phraseId}`);
        const soundsPerPhraseRef = this.db.database.ref(`/soundsPerPhrase/`);

        // assumes init has been called for current phrase.
        // if (!!this.soundCountPerPhrase) {
            const soundKeysArray = [];
            soundsPerPhraseRef.orderByKey().equalTo(phraseId).once('child_added', snapshot => {
                const numSounds = snapshot.numChildren();
                const sounds = snapshot.val();

                console.log('sounds', sounds);
                _.forEach(sounds, sound => {
                    soundKeysArray.push(sound.id);

                    if (soundKeysArray.length === numSounds) {
                        this.getSoundsByKeys(soundKeysArray, callback);
                        // callback(soundsArray);
                    }
                });

                // soundsArray.push(new Sound(snapshot.key, sound.writtenForm, sound.audio, sound.notes, sound.speechAspects));
                // console.log('soundsArray length: ' + soundsArray.length + ' numSounds: ' + numSounds);
                // if (soundsArray.length === numSounds) {
                //     console.log('triggering callback');
                //     callback(soundsArray);
                // }
            });
        // } else {
        //     console.log('You need to run init on the current phrase.');
        // }
    }

    getSoundsByKeys(keys, callback?) {
        const keysArray = _.isArray(keys) ? keys : [keys];
        const soundsArray = [];

        _.forEach(keysArray, key => {
            const ref = this.db.database.ref(`/sounds/`);

            ref.orderByKey().equalTo(key).once('child_added', snapshot => {
                const sound = snapshot.val();
                console.log('sound', sound);
                soundsArray.push(new Sound(snapshot.key, sound.writtenForm, sound.audio, sound.notes, sound.speechAspects));

                if (soundsArray.length === keysArray.length) {
                    callback(soundsArray, keysArray);
                }
            });
        });
    }

    cacheSoundKey(objKey, soundKey) {
        this.sharedDataService.set(objKey, soundKey);
    }

    getCachedSoundKey(objKey) {
        return this.sharedDataService.get(objKey);
    }
}

import { Component, Input, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { SoundsService } from '../sounds.service';
import { PhrasesService } from '../services/phrases/phrases.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Observable } from '@firebase/util';
import { HighlightSubstringPipe } from '../pipes/highlight-substring.pipe';
import 'rxjs/add/operator/switchMap';
import { SoundsComponent } from '../sounds/sounds.component';
import * as _ from 'lodash';
import { Sound } from '../types/sound';
import { Phrase } from '../types/phrase';
import { Subscription } from 'rxjs/Subscription';
import { SharedDataService } from '../services/shared-data/shared-data.service';
import { TestQuestionsService } from '../services/questions/test-questions/test-questions.service';
import { Subject } from 'rxjs/Subject';


@Component({
    selector: 'app-sound-to-study',
    templateUrl: './sound-to-study.component.html',
    styleUrls: ['./sound-to-study.component.scss']
})

export class SoundToStudyComponent implements OnInit, OnDestroy {
    phraseId: string;
    soundId: string;
    phrase: Phrase;
    sounds: Sound[];
    currentSound: Sound;
    previousSound: Sound;
    nextSound: Sound;
    pmSubscription: Subscription;
    testQuestionsReady: Subject<boolean>;
    testQuestionsSub: Subscription;
    firstQuestionPath: any; // if isReview is false or undefined, we'll load the first question.
    nextQuestionPath: any; // if isReview is true, we'll load the next question.
    isReview: boolean;

    constructor(
        private soundsService: SoundsService, private elRef: ElementRef,
        private phrasesService: PhrasesService, private route: ActivatedRoute,
        private router: Router, private sharedDataService: SharedDataService,
        private testQuestionsService: TestQuestionsService) {

        this.phraseId = this.route.snapshot.paramMap.get('phraseId');
        this.soundId = this.route.snapshot.paramMap.get('soundId');
    }

    ngOnInit() {
        // do some stuff on load like get total number of sounds for the current phrase.
        this.soundsService.init(this.phraseId);

        this.phrasesService.getPhraseByKey(this.phraseId, phrase => {
            this.phrase = phrase;
            this.sharedDataService.set('currentPhrase', this.phrase);

            // subscribe to the thing that changes: the sound ID in the URL.
            this.pmSubscription = this.route.paramMap.subscribe(paramMap => {
                const soundKey = paramMap.get('soundId');

                this.soundsService.getSoundByKey(soundKey, sound => {
                    this.currentSound = sound;
                    // console.log('current sound:', this.currentSound);

                    if (!this.route.snapshot.queryParamMap.get('review')) {
                        // Load the next and previous sounds for pagination.
                        this.preloadPrevious();
                        this.preloadNext();
                    } else {
                        console.log('It\'s a review of a previously studied sound.');
                        this.isReview = true;
                        this.prepareNextRoute();
                    }
                });
            });
        });
    }

    /*
        preloadPrevious
        Get data for the sound whose order property is one less than the current sound.

        @return sound: Sound, or null if looking at the first sound.
    */
    preloadPrevious() {
        this.soundsService.getPreviousSoundInPhrase(this.phrase.$key, this.currentSound.$key, sound => {
            // console.log(sound);
            this.previousSound = sound;
        });
    }

    /*
        preloadNext
        Get data for the sound whose order property is one more than the current sound.

        @return sound: Sound, or null if looking at the last sound.
    */
    preloadNext() {
        this.soundsService.getNextSoundInPhrase(this.phrase.$key, this.currentSound.$key, sound => {
            console.log('NEXT SOUND IS:', sound);
            this.nextSound = sound;

            if (this.nextSound === null) {
                this.testQuestionsService.initState(this.phrase.$key);

                this.testQuestionsSub = this.testQuestionsService.ready$.subscribe(ready => {
                    this.firstQuestionPath = this.testQuestionsService.getSetNextQuestion();

                    console.log('First Q is...:', this.firstQuestionPath);
                });
            }
        });
    }

    goToFirstQuestion() {
        if (this.firstQuestionPath) {
            this.router.navigate(this.firstQuestionPath);
        } else {
            prompt('There is no first question.');
        }
    }

    prepareNextRoute() {
        if (!!this.testQuestionsService.ready) {
            console.log('Preparing path for next question...');
            this.nextQuestionPath = this.testQuestionsService.getSetNextQuestion();
            console.log('Next Q will be:', this.nextQuestionPath);
        }
    }

    /*
        Unsubscribe from subscriptions when the component is destroyed to prevent memory leaks.
        Note: when you have a variable of type Observable you can use the async pipe in the view
        which will unsubscribe automatically.
    */
    ngOnDestroy() {
        if (this.testQuestionsSub) {
            this.testQuestionsSub.unsubscribe();
        }
        this.pmSubscription.unsubscribe();
        // this.testQuestionsService.initState(this.phrase.$key);
    }

}


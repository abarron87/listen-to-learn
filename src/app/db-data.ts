export const dbData = {
    "phrases": [
        {
            "writtenForm": "Yea he's off his tits!",
            "explanation": "To be very drunk.",
            "audio": "some-audio-file",
            "video": "some-video-file",
            "sounds": [
                {
                    "writtenForm": "yea he's",
                    "audio": "some-audio-file",
                    "order": "0",
                    "notes": [
                        {
                            "note": "Here we don't pronounce the 'h' but keep the rest of the sound of 'he\'s'."
                        }
                    ],
                    "speechAspects": [
                        {
                            "name": "Elision (hiding) of /h/"
                        },
                        {
                            "name": "Informal speech"
                        }
                    ]
                },
                {
                    "writtenForm": "off his",
                    "audio": "https://firebasestorage.googleapis.com/v0/b/listen-to-learn-c825c.appspot.com/o/Sounds%2Foffiz.mp3?alt=media&token=e1df0792-2dcc-424f-8ea9-9109e3311db3",
                    "order": "1",
                    "notes": [
                        {
                            "note": "Here we remove the letter 'h' and so 'f' is followed by a vowel sound (/ɪ/). So we can pronounce both words as if they were one."
                        }
                    ],
                    "speechAspects": [
                        {
                            "name": "Elision (hiding) of /h/",
                        },
                        {
                            "name": "Catenation (linking) of a consonant sound to a vowel sound"
                        },
                        {
                            "name": "Informal speech"
                        }
                    ]
                },
                {
                    "writtenForm": "tits",
                    "audio": "some-audio-file",
                    "order": "2",
                    "notes": [
                        {
                            "note": "Tits is often a slang word for breasts."
                        }
                    ],
                    "speechAspects": [
                        {
                            "name": "Slang"
                        }
                    ]
                }
            ]
        },
        {
            "writtenForm": "I'll have a glass of water. Cheers.",
            "audio": "some-audio-file",
            "video": "some-video-file",
            "sounds": [
                {
                    "writtenForm": "I'll have",
                    "audio": "some-audio-file",
                    "order": "0",
                    "notes": [
                        {
                            "note": "Contraction with an apostrophe is extremely common and more natural in most situations."
                        }
                    ],
                    "speechAspects": [
                        {
                            "name": "Elision (hiding) of /h/"
                        },
                        {
                            "name": "Weak forms as schwa (/ə/)"
                        },
                        {
                            "name": "Contraction"
                        }
                    ]
                },
                {
                    "writtenForm": "a glass of",
                    "audio": "some-audio-file",
                    "order": "1",
                    "notes": [
                        {
                            "note": "In the middle of a sentence /eɪ/ (a) and /ɒv/ (of) are in their weak forms: /ə/ (schwa). Sometimes you will hear 'of' pronounced with the /v/ sound (/əv/) and sometimes not. Here it is not pronounced and is simply /ə/."
                        }
                    ],
                    "speechAspects": [
                        {
                            "name": "Weak forms as schwa (/ə/)"
                        }
                    ]
                },
                {
                    "writtenForm": "water. Cheers",
                    "audio": "some-audio-file",
                    "order": "2",
                    "notes": [
                        {
                            "note": "Often, the 't' is the middle of a word is not pronounced at all. It's replaced by what we call a glottal stop. The pronunciation of this word is extremely British."
                        }
                    ],
                    "speechAspects": [
                        {
                            "name": "Glottal T"
                        },
                        {
                            "name": "Informal speech"
                        }
                    ]
                }
            ]
        }
    ]
}
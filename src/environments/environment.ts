// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDnTpcs6iksdkOSAUIZrXMAJ1GLwHglZmw",
    authDomain: "listen-to-learn-c825c.firebaseapp.com",
    databaseURL: "https://listen-to-learn-c825c.firebaseio.com",
    projectId: "listen-to-learn-c825c",
    storageBucket: "listen-to-learn-c825c.appspot.com",
    messagingSenderId: "782989005354"
  }
};

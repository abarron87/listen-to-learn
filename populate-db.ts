import { database, initializeApp } from 'firebase';
import { environment } from './src/environments/environment';
import { dbData } from './src/app/db-data';

console.log('Initializing Firebase database...');

initializeApp(environment.firebase);

const phrasesRef = database().ref('phrases');
const soundsRef = database().ref('sounds');

// loop over all phrases and create one object for each in the database.
dbData.phrases.forEach( phrase => {
    console.log('Adding phrase', phrase.writtenForm);

    const phraseDbData = {
        writtenForm: phrase.writtenForm,
        audio: phrase.audio,
        video: phrase.video
    };

    if (!!phrase.explanation) {
        phraseDbData.explanation = phrase.explanation;
    }

    const phraseRef = phrasesRef.push(phraseDbData);

    const soundKeysPerPhrase = [];

    // loop through each sound in each phrase and add it to the database.
    // Add the newly created phrase push id to the sound.
    phrase.sounds.forEach((sound:any) => {
        console.log('Adding sound', sound.writtenForm);

        const soundDbData = {
            writtenForm: sound.writtenForm,
            audio: sound.audio
        };

        if (!!sound.notes) {
            soundDbData.notes = sound.notes;
        }

        if (!!sound.speechAspects) {
            soundDbData.speechAspects = sound.speechAspects;
        }

        soundKeysPerPhrase.push({ key: soundsRef.push(soundDbData).key, order: sound.order});
    });


    // Create a node to connect sounds to phrases.
    const association = database().ref('soundsPerPhrase');

    // create one new entry in the association for each phrase.
    const soundsPerPhrase = association.child(phraseRef.key);

    // loop over each sound key and add it to the new phrase entry in the association node.
    soundKeysPerPhrase.forEach(soundKey => {
        console.log('Adding sound to phrase.');

        // create new entry in the current phrase.
        const soundPhraseAssociation = soundsPerPhrase.child(soundKey.key);

        // set value to true. we only care about the key.
        soundPhraseAssociation.set(soundKey.order);
    });
});



import { database, initializeApp } from 'firebase';
import { environment } from './src/environments/environment';
import { dbData } from './src/app/db-data-multi-choice';

console.log('Initializing Firebase database...');

initializeApp(environment.firebase);

const multipleChoiceRef = database().ref('multipleChoice');

dbData.forEach(choice => multipleChoiceRef.push(choice));
